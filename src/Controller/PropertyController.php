<?php
namespace App\Controller;


use App\Entity\Property;
use App\Repository\PropertyRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class PropertyController extends AbstractController {
/**
 * @Route("/biens", name = "property.index")
 */
  public function index(PropertyRepository $proprepo): Response
  {

    $properties = $proprepo->findLatest();
    return $this->render("pages/property.html.twig",[
     "properties" => $properties

    ]);
  }
  /**
   * @Route("/biens/{slug}-{id}" , name= "property.show",requirements={"slug" : "[a-z0-9\-]*"})
   * 
   */
  public function show(int $id,PropertyRepository $prorepo,String $slug) :Response
  {
    $property = $prorepo->find($id);
    if($property->getSlug() !== $slug)
    return $this->redirectToRoute('property.show',[
      'id' => $property->getId(),
      'slug'=> $property->getSlug()
    ]);
     return $this->render("pages/chosedproperty.html.twig",[
       "property" => $property
     ]);
  }


}

